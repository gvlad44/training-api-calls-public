const express = require("express");
const app = express();

const dataController = {
  getData: async (req, res) => {},

  postData: async (req, res) => {},

  patchData: async (req, res) => {},

  deleteData: async (req, res) => {},
};

app.get("/get", dataController.getData);
app.post("/post", dataController.postData);
app.patch("/patch", dataController.patchData);
app.delete("/delete", dataController.deleteData);

app.listen(3000);
